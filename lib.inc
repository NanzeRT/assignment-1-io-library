SYSCALL_EXIT  equ 60
STDOUT_FILE_DESCRIPTOR  equ 1
SYSCALL_WRITE  equ 1

section .rodata
read_failed: db "read failed because of unknown error", 0xA, 0

section .text

; globals
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int 
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int 
global string_copy
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    cmp byte [rdi], 0
    je .end
    inc rax
    inc rdi
    jmp .loop
    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT_FILE_DESCRIPTOR
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT_FILE_DESCRIPTOR
    mov rsi, rsp
    mov rdx, 1
    syscall
    add rsp, 8
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r10, rsp
    sub rsp, 32
    dec r10
    mov [r10], byte 0
    mov rax, rdi
    mov rcx, 10
    .loop:
    xor rdx, rdx
    div rcx
    add dl, '0'
    dec r10
    mov [r10], dl
    test rax, rax
    jne .loop
    mov rdi, r10 
    call print_string
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .print 
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .print: 
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
    mov al, [rdi]
    mov ah, [rsi]
    cmp al, ah
    je .cont
    xor rax, rax
    ret
    .cont:
    test al, al
    je .end
    inc rdi
    inc rsi
    jmp .loop
    .end:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    je .end
    jge .success
    mov rdi, read_failed
    mov r12, rax
    call print_string
    mov rdi, r12
    call exit
    .success:
    xor rax, rax
    mov al, [rsp]
    .end:
    inc rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rdi
    push rdi
    add rdi, rsi
    push rdi
    test rsi, rsi
    je .fail
    .skip:
    call read_char
    test rax, rax
    je .fail
    cmp al, ` `
    je .skip
    cmp al, `\t`
    je .skip
    cmp al, `\n`
    je .skip
    .read:
    mov rdi, [rsp+8]
    mov [rdi], al
    inc rdi
    cmp rdi, [rsp]
    je .fail
    mov [rsp+8], rdi
    call read_char
    test al, al
    je .end
    cmp al, ` `
    je .end
    cmp al, `\t`
    je .end
    cmp al, `\n`
    je .end
    jmp .read
    .end:
    mov rdi, [rsp+8]
    mov [rdi], byte 0
    mov rax, [rsp+16]
    mov rdx, [rsp+8]
    sub rdx, rax
    add rsp, 24
    ret
    .fail:
    xor rax, rax
    xor rdx, rdx
    add rsp, 24
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку из stdin,
; Останавливается и возвращает 0 если строка слишком большая для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к строке нуль-терминатор
read_line:
    push rdi
    push rdi
    add rdi, rsi
    push rdi
    test rsi, rsi
    je .fail
    .read:
    call read_char
    test al, al
    je .end
    cmp al, `\n`
    je .end
    mov rdi, [rsp+8]
    mov [rdi], al
    inc rdi
    cmp rdi, [rsp]
    je .fail
    mov [rsp+8], rdi
    jmp .read
    .end:
    mov rdi, [rsp+8]
    mov [rdi], byte 0
    mov rax, [rsp+16]
    mov rdx, [rsp+8]
    sub rdx, rax
    add rsp, 24
    ret
    .fail:
    xor rax, rax
    xor rdx, rdx
    add rsp, 24
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rdi
    push rdi
    push 0
    .loop:
    ; call read_char
    mov rdi, [rsp+8]
    mov al, [rdi]
    sub al, '0'
    cmp al, 9
    ja .end
    inc dword [rsp+8]
    xor rdi, rdi
    mov dil, al
    mov rax, [rsp]
    mov rsi, 10
    mul rsi
    add rax, rdi
    mov [rsp], rax
    jmp .loop
    .end:
    pop rax
    pop rdx
    pop rdi
    sub rdx, rdi
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp [rdi], byte '-'
    je .neg
    jmp parse_uint
    .neg:
    inc rdi
    call parse_uint
    neg rax
    add rdx, 1
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    add rdx, rsi
    .loop:
    cmp rsi, rdx
    je .fail
    mov al, [rdi]
    mov [rsi], al
    inc rdi
    inc rsi
    test al, al
    jne .loop
    sub rsi, rdx
    mov rax, rsi
    ret
    .fail:
    xor rax, rax
    ret

